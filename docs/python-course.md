<!-- Document Title -->
Documentation based on knowledge graspped from the course "Complete Python Mastery" by codewithmosh

---

<!-- Metadata -->
**Metadata:**
| Key       | Value                                                                                                                                                                                                                                                                                                           |
| :-------- | :-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Title     | Documentation based on knowledge graspped from the course "Complete Python Mastery" by codewithmosh(Python Quick-reference guide / revision guide / Cheat-sheet                                                                                                                                                 |
| Author    | Mrunal Nachankar <mrunalmn4@gmail.com>                                                                                                                                                                                                                                                                          |
| Reference | <ul><li>https://www.youtube.com/watch?v=_uQrJ0TkZlc&list=PLTjRvDozrdlxj5wgH4qkvwSOdHLOCx10f</li><li> https://www.youtube.com/watch?v=kqtD5dpn9C8&list=PLTjRvDozrdlxj5wgH4qkvwSOdHLOCx10f&index=18</li><li>https://www.youtube.com/watch?v=f79MRyMsjrQ&list=PLTjRvDozrdlxj5wgH4qkvwSOdHLOCx10f&index=2</li></ul> |


---
**Todo / Pending list / Future Scope:**
- [ ] Documenting everything
- [ ] Adding output

---

**Table of Contents:**
- [1. Basic information (Worth reading for knowledge)](#1-basic-information-worth-reading-for-knowledge)
  - [1.1. Python - introduction:](#11-python---introduction)
  - [1.2. Python (What?):](#12-python-what)
  - [1.3. Reasons (Why?):](#13-reasons-why)
  - [1.4. Versions:](#14-versions)
  - [1.5. Offical links:](#15-offical-links)
  - [1.6. Python - Open-source](#16-python---open-source)
  - [1.7. Python implementation](#17-python-implementation)
  - [1.8. Python execution -> Platform Independent](#18-python-execution---platform-independent)
  - [1.9. Basic programming language terminologies:](#19-basic-programming-language-terminologies)
  - [1.10. Vscode plugins/extensions:](#110-vscode-pluginsextensions)
- [2. Primitive types (Premitive datatypes)](#2-primitive-types-premitive-datatypes)
  - [2.1. Baiscs](#21-baiscs)
    - [2.1.1. Comments](#211-comments)
    - [2.1.2. Calling a function](#212-calling-a-function)
  - [2.2. Variables](#22-variables)
    - [2.2.1. variable names](#221-variable-names)
  - [2.3. string](#23-string)
    - [2.3.1. Length of string](#231-length-of-string)
    - [2.3.2. Specific character via index](#232-specific-character-via-index)
    - [2.3.3. Slice -> substring](#233-slice---substring)
    - [2.3.4. Escape sequences](#234-escape-sequences)
    - [2.3.5. Formatted strings](#235-formatted-strings)
    - [2.3.6. string functions also called as string methods](#236-string-functions-also-called-as-string-methods)
      - [2.3.6.1. upper](#2361-upper)
      - [2.3.6.2. lower](#2362-lower)
      - [2.3.6.3. title](#2363-title)
      - [2.3.6.4. swapcase](#2364-swapcase)
      - [2.3.6.5. strip](#2365-strip)
      - [2.3.6.6. lstrip](#2366-lstrip)
      - [2.3.6.7. rstrip](#2367-rstrip)
      - [2.3.6.8. find](#2368-find)
      - [2.3.6.9. replace](#2369-replace)
      - [2.3.6.10. in](#23610-in)
      - [2.3.6.11. not in](#23611-not-in)
      - [2.3.6.12. split](#23612-split)
  - [2.4. Numbers](#24-numbers)
    - [2.4.1. Integer](#241-integer)
    - [2.4.2. Float](#242-float)
    - [2.4.3. Complex number](#243-complex-number)
    - [2.4.4. Standard Arithematic operations](#244-standard-arithematic-operations)
      - [2.4.4.1. Addition -> +](#2441-addition---)
      - [2.4.4.2. Subtraction -> -](#2442-subtraction----)
      - [2.4.4.3. Multiplication -> *](#2443-multiplication---)
      - [2.4.4.4. Division: Float value -> /](#2444-division-float-value---)
      - [2.4.4.5. Division: Integer value -> //](#2445-division-integer-value---)
      - [2.4.4.6. Remainder -> %](#2446-remainder---)
      - [2.4.4.7. Exponent -> **  (10 ** 3  -> 1000 ```python](#2447-exponent-----10--3----1000-python)
      - [2.4.4.8. TODO: Mrunal : What is it?](#2448-todo-mrunal--what-is-it)
      - [2.4.4.9. TODO: Mrunal : What is it?](#2449-todo-mrunal--what-is-it)
    - [2.4.5. Incrementation tradtional and augmented assignment operator](#245-incrementation-tradtional-and-augmented-assignment-operator)
      - [2.4.5.1. Incrementation tradtional](#2451-incrementation-tradtional)
      - [2.4.5.2. Augmented assignment operator (Addition operations) (or increment)](#2452-augmented-assignment-operator-addition-operations-or-increment)
      - [2.4.5.3. Augmented assignment operator (Subtraction operations) (or decrement)](#2453-augmented-assignment-operator-subtraction-operations-or-decrement)
      - [2.4.5.4. Augmented assignment operator (Multiplication operations)](#2454-augmented-assignment-operator-multiplication-operations)
      - [2.4.5.5. Augmented assignment operator (Division operations)](#2455-augmented-assignment-operator-division-operations)
      - [2.4.5.6. Augmented assignment operator (Division operations)](#2456-augmented-assignment-operator-division-operations)
      - [2.4.5.7. Augmented assignment operator (Remainder operations)](#2457-augmented-assignment-operator-remainder-operations)
      - [2.4.5.8. Augmented assignment operator (Exponent operations)](#2458-augmented-assignment-operator-exponent-operations)
      - [2.4.5.9. Incorrect : common mistake](#2459-incorrect--common-mistake)
    - [2.4.6. Number functions also called as number methods](#246-number-functions-also-called-as-number-methods)
      - [2.4.6.1. round](#2461-round)
      - [2.4.6.2. math module/object](#2462-math-moduleobject)
      - [2.4.6.3. TODO: Munal : Add more math functions](#2463-todo-munal--add-more-math-functions)
  - [2.5. Input function and type conversion](#25-input-function-and-type-conversion)
    - [2.5.1. Input function -> Always saves as string object/datatype](#251-input-function---always-saves-as-string-objectdatatype)
    - [2.5.2. Type conversion](#252-type-conversion)
      - [2.5.2.1. Convert to string value](#2521-convert-to-string-value)
      - [2.5.2.2. Convert to integer / int value](#2522-convert-to-integer--int-value)
      - [2.5.2.3. Convert to float value](#2523-convert-to-float-value)
      - [2.5.2.4. Convert to boolean / bool value](#2524-convert-to-boolean--bool-value)
      - [2.5.2.5. Bool value converstion have falsey values (#Tags: #False, #Falsey, #Falsy)](#2525-bool-value-converstion-have-falsey-values-tags-false-falsey-falsy)
- [3. Control Flow](#3-control-flow)
  - [3.1. comparision operators (with numbers)](#31-comparision-operators-with-numbers)
    - [3.1.1. greater than -> >](#311-greater-than---)
    - [3.1.2. greater than or equal to -> >=](#312-greater-than-or-equal-to---)
    - [3.1.3. less than -> <](#313-less-than---)
    - [3.1.4. less than or equal to -> <=](#314-less-than-or-equal-to---)
    - [3.1.5. equal to -> ==](#315-equal-to---)
    - [3.1.6. not equal to -> !=](#316-not-equal-to---)
    - [3.1.7. order -> Unicode value -> ord](#317-order---unicode-value---ord)
  - [3.2. comparision operators (with strings)](#32-comparision-operators-with-strings)
    - [3.2.1. greater than -> >](#321-greater-than---)
    - [3.2.2. greater than or equal to -> >=](#322-greater-than-or-equal-to---)
    - [3.2.3. less than -> <](#323-less-than---)
    - [3.2.4. less than or equal to -> <=](#324-less-than-or-equal-to---)
    - [3.2.5. equal to -> ==](#325-equal-to---)
    - [3.2.6. not equal to -> !=](#326-not-equal-to---)
  - [3.3. Print all unicode values         (#Tags: #All_Unicode_Values, #Unicode, #Unicode_Values)](#33-print-all-unicode-values---------tags-all_unicode_values-unicode-unicode_values)
  - [3.4. Conditional statements](#34-conditional-statements)
    - [3.4.1. if statement](#341-if-statement)
    - [3.4.2. elif statement](#342-elif-statement)
    - [3.4.3. if...elif...else statement](#343-ifelifelse-statement)
  - [3.5. Ternary Operator           (#Tags: #If_Shorthand, #Short_Hand)](#35-ternary-operator-----------tags-if_shorthand-short_hand)
    - [3.5.1. Example 1: if](#351-example-1-if)
    - [3.5.2. Example 2: nested if](#352-example-2-nested-if)
    - [3.5.3. TODO: Mrunal : Check whether elif is allowed ??? (Possibly No)](#353-todo-mrunal--check-whether-elif-is-allowed--possibly-no)
  - [3.6. Chaining comparision Operator   (#Tags: #Comparsion_Operator_Shorthand, #shorthand)](#36-chaining-comparision-operator---tags-comparsion_operator_shorthand-shorthand)
    - [3.6.1. Example: if](#361-example-if)
  - [3.7. Logical Operator (and , or , not)](#37-logical-operator-and--or--not)
  - [3.8. Short-circuit Evaluation (Logical operators are short-circuit -> if one of the operends {of logical operator} is false, evalutation stops)](#38-short-circuit-evaluation-logical-operators-are-short-circuit---if-one-of-the-operends-of-logical-operator-is-false-evalutation-stops)
  - [3.9. for Loops](#39-for-loops)
    - [3.9.1. Repeat n number of times](#391-repeat-n-number-of-times)
    - [3.9.2. Repeat between 2 values](#392-repeat-between-2-values)
    - [3.9.3. Repeat between 2 values with different increment/decrement value than default(+1/-1) as step](#393-repeat-between-2-values-with-different-incrementdecrement-value-than-default1-1-as-step)
    - [3.9.4. Repeat with negative increment value (decrement value)](#394-repeat-with-negative-increment-value-decrement-value)
  - [3.10. for...else Loops and break statements            (#Tags: #Loop_Break, #Break)](#310-forelse-loops-and-break-statements------------tags-loop_break-break)
    - [3.10.1. comes out of the loop -> no more iterations break and goto next statements following loop](#3101-comes-out-of-the-loop---no-more-iterations-break-and-goto-next-statements-following-loop)
  - [3.11. Nested loops -> Loops into loops](#311-nested-loops---loops-into-loops)
    - [3.11.1. outer loop](#3111-outer-loop)
  - [3.12. Iterables -> objects that doesn't need explicit increment of variables](#312-iterables---objects-that-doesnt-need-explicit-increment-of-variables)
    - [3.12.1. Iterables with range (range -> complex type)](#3121-iterables-with-range-range---complex-type)
    - [3.12.2. Iterables with string (string -> primitive type)](#3122-iterables-with-string-string---primitive-type)
    - [3.12.3. Iterables with array (array -> complex type)](#3123-iterables-with-array-array---complex-type)
  - [3.13. While loops](#313-while-loops)
  - [3.14. Infinte loops -> Infinte iterations            (#Tags: #Infinte_Loop, #Bad_Practice)](#314-infinte-loops---infinte-iterations------------tags-infinte_loop-bad_practice)
  - [3.15. Exercise](#315-exercise)
    - [3.15.1. Solution](#3151-solution)


---


<!-- Section: Basic information (Worth reading for knowledge) -->

# 1. Basic information (Worth reading for knowledge)

+ Basic help about following documentation (in this file):
  < > -> are used in documention in this file (comment) for a spaceholder (Its like variable and not actual text)

## 1.1. Python - introduction:
World's fastest growing and most popular programming languag amongst
  + Software Engineers
  + Mathematicians
  + Data Analysts
  + Scientists
  + Accountants
  + Network Engineers
  + Hackers
  + Kids

## 1.2. Python (What?):
  + powerful... and fast
  + plays well with others
  + runs everywhere
  + friendly & easy to learn
  + Open.

## 1.3. Reasons (Why?):
  + Smiple and smallest learning curve
    + Solve complex problems in less time with few lines of code
  + Multi-purpose language: -> Application:
    + Data Analysis
    + AI/ML
    + Automation
    + Web Apps
    + Mobile Apps
    + Desktop Apps
    + Testing
    + Hacking
  + High-level language -> Complex task like memory mangement is already taken care
  + Cross-platform -> Build an run apps on Mac, Linux and Windows
  + Huge Community -> Whenever your are stuck, help is always around all over the globe
  + Large Ecosystem -> Enormous Libraries, framworks or tools

## 1.4. Versions:
  + Python2 -> Legacy version -> Support till 2020
  + Python3 -> For future

## 1.5. Offical links:
  + Website -> https://www.python.org/
  + Download -> https://www.python.org/downloads/
  + Documentation -> https://www.python.org/doc/
  + Community -> https://www.python.org/community/

## 1.6. Python - Open-source
  Python is developed under an OSI-approved open source license, making it freely usable and distributable, even for commercial use. Python's license is administered by the Python Software Foundation.

## 1.7. Python implementation
  Program that understand rules and specification in language and execute the code
  + CPython
    + program written in C
    + default (updates - first supported)
  + Jython
    + program written in Java
    + import java code in python
  + Ironython
    + program written in C#
    + import java code in python
  + Pypy
    + program written in Subset of python

## 1.8. Python execution -> Platform Independent
  + Complier
    + converts high level instruction in code to low level(Machine code) instructions
      + Machine code is specific to type of processors (Means it is platform dependent)
  + Python code -> CPython (tool) compiles it into "Python + Bytecode" -> Python Bytecode -> Python Virtual Machine (tool) convert "Python Bytecode" to "Machine code" -> Machine code and execute it (Means it is platform independent thanks to CPython and Python Virtual Machine)

## 1.9. Basic programming language terminologies:
  + language -> tool that helps in communication
  + programming language (also reffered as language in this document) -> tool that helps in communication with computer
  + Syntax -> the correct way of writing of code (Like grammer in english language)
  + Compiler -> Program that converts high level language (human readable) to low level (machine readable) language
  + Interpreter -> Program that executes code writen in code files
  + Editor -> Tool to type the code (add, modify, delete and Save)
  + IDE (Integrated Developement Environment) -> code editor with fancy features like
      + Autocomplete -> as you type code it completes your code (automatically completes the code) {Productivity Booster}
      + Debugging -> Finding and fixing bugs (with breakpoints and step by step navigation)
      + Testing -> Test the code (before depoyment)
  + Developement -> Process of creating / modifying the code
  + Deployment -> Making the final app/product (after code developmet is successful), making it available for end users (consumers) {in the form of web app or platform/standalone app or an api}
  + Staging -> Testing the app with the system configuration close to production environment
  + Production -> The environment where final version of app is available for users to use/explore/consume
  + Expression ->  Piece of code that produces a value. (2 + 2)
  + Result / Output ->  Full vaule obtained after executing the piece of code or any single expression
  + Input -> instructions or values passed to program
  + Interactive shell / Python shell -> cpython terminal
  + Arguments -> Input value to functions

+ run/execute basic python file ->  python<version> filename.py

## 1.10. Vscode plugins/extensions:
  + python -> Adds features like
    + Linting -> Analyzing code for potential errors (Syntax errors)
    + Autocompletion -> as you type code it completes your code
    + Unit Testing -> writing the bunch of tests. executing testing automated manner to make sure code is working correctly
    + Debugging -> Finding and fixing bugs/errors
    + Code Formatting and Styling -> make the code clean and presentable so that code is easy to read and uniformity
    + Code Snippets -> reusable code block
  + pylint (suggested after python extension is installed) -> Linting use (Ref: https://pypi.org/project/pylint/ and https://www.pylint.org/)
  + autopep8 (suggested after python and pylint extension is installed) -> Code Formatting and Styling (Ref: https://www.python.org/dev/peps/ for peps guide)
  + Code Runner -> Many language support




# 2. Primitive types (Premitive datatypes)

## 2.1. Baiscs

### 2.1.1. Comments
These are comments in python (code not to be executed)
```python
#   Single line(Full line) comment
print("Test")               # In-line comment

'''
Multi-line
comments
'''
```

### 2.1.2. Calling a function
```python
Syntax:
<function_name>(<arguments>)
```

+ Function -> print()

  prints on console

```python
print("\n*** Basic Hello World using print function ***")
# prints ->
#
# *** Basic Hello World using print function ***

print("Hello World!!!")
# prints ->
# Hello World!!!

print("$" * 5)
# prints ->
# $$$$$
```


## 2.2. Variables
A placeholder/container to hold some value
### 2.2.1. variable names
  + must be meaningful and descriptive
  + must start with small character(alphabets) and eep everything in small case
  + seperate words using underscode "_"
  + space around equal to sign " = "

```python
# integer variable
student_count = 1000

# floting point variable
rating = 4.99

# boolean variable (accepts True or False -> These are case sensitive)
is_published = True

# string variable (must use single{'abc'} or multiple{"abc"} quotes and for multi-line string value triple single{'''abc'''} or triple double{"""abc"""} qoutes must be used)
course_name = 'Python Programming'
course_name = "Python Programming"

mail_body = '''
This is
an example
to show
multi-line string value
'''
mail_body = """
This is
an example
to show
multi-line string value
"""
```


## 2.3. string
```python
# Define variable named course
# Assign value to it as - Python programmin
course = "Python programming"
```

### 2.3.1. Length of string
```python
# Length of string
print(len(course))
```

### 2.3.2. Specific character via index
+ Left -> -> Right :
  + index starts front/left with 0 and then 1 then 2 and so on till 1 less than the length of string
+ Left <- <- Right :
  + index starts behind/right with -1 and then -2 then -3 and so on till 0
```python
# Specific character via index
print(course[0])
print(course[1])
print(course[5])
print(course[-12])
print(course[-5])
print(course[-1])
```

### 2.3.3. Slice -> substring
+ extracting substring from the string
+ remember highest value of right side of colon is not considered in selection.
  + For example in [5:9], 9 index will not be considered in selection
```python
# Slice -> substring -> extracting substring from the string
# First 5 characters
print(course[0:5])
print(course[1:5])
print(course[2:])
print(course[:8])
print(course[:])
```

### 2.3.4. Escape sequences
```python
#   Escape character \ and Escape sequence \" -> prints "
print("Python \"programming")

#   Escape character \ and Escape sequence \' -> prints '
print("Python \'programming")

#   Escape character \ and Escape sequence \\ -> prints \\
print("Python \\programming")

#   Escape character \ and Escape sequence \n -> prints new line
print("Python \nprogramming")


#   Escape character \ and Escape sequence \t -> prints tab
print("Python \t\tprogramming")
```

### 2.3.5. Formatted strings
```python
# Formatted strings
first = "Munna"
last = "Bhai IT"

# concatination : tradional approach
print(first + " " + last)

# Formatted strings -> Use parenthesis brackets { } to include variables function or expression
print(f"{first} {last}")

print(f"{first} {last} {len(first)} {5 + 9} { 9 <= 8}")
```

### 2.3.6. string functions also called as string methods

#### 2.3.6.1. upper
```python
# string functions also called as string methods
course = " Python programMing   "

# converts in upper case
print(course.upper())
```

#### 2.3.6.2. lower
```python
# string functions also called as string methods
course = " Python programMing   "

# converts in lower case
print(course.lower())
```

#### 2.3.6.3. title
```python
# string functions also called as string methods
course = " Python programMing   "

# converts in title case
print(course.title())
```

#### 2.3.6.4. swapcase
```python
# string functions also called as string methods
course = " Python programMing   "

# converts in inverse/swap the exsiting case
print(course.swapcase())
```

#### 2.3.6.5. strip
```python
# string functions also called as string methods
course = " Python programMing   "

# converts in strips/trims empty spaces on both the sides
print(course.strip())
```

#### 2.3.6.6. lstrip
```python
# string functions also called as string methods
course = " Python programMing   "

# converts in strips/trims empty spaces on the left sides
print(course.lstrip())
```

#### 2.3.6.7. rstrip
```python
# string functions also called as string methods
course = " Python programMing   "

# converts in strips/trims empty spaces on the right sides
print(course.rstrip())
```

#### 2.3.6.8. find
```python
# string functions also called as string methods
course = " Python programMing   "

# returns -1 if found prints index of first character
print(course.find("pro"))

# returns -1 if not found (Not found because python is case sensitive)
print(course.find("Pro"))
```

#### 2.3.6.9. replace
```python
# string functions also called as string methods
course = " Python programMing   "

# replaces the character with the another one
print(course.replace("p", "j"))
```

#### 2.3.6.10. in
```python
# string functions also called as string methods
course = " Python programMing   "

# returns True/False if found or not found respectively
print("pro" in course)
```

#### 2.3.6.11. not in
```python
# string functions also called as string methods
course = " Python programMing   "

# returns False/True if found or not found respectively
print("pro" not in course)
```

#### 2.3.6.12. split
```python
# string functions also called as string methods
course = " Python programMing   "

# returns array of words as elements which are space seperated
print(course.split())
```

## 2.4. Numbers
print("\n*** Numbers ***")

### 2.4.1. Integer
```python
number_integer = 34
```

### 2.4.2. Float
```python
number_float = 34.2346
```

### 2.4.3. Complex number
```python
number_complex = 34 + 23j
```
```python
# Printing the number variables
print(
    f"integer:{number_integer} \tfloat:{number_float} \tcomplex:{number_complex}")
```

### 2.4.4. Standard Arithematic operations
print("\n*** Standard Arithematic operations ***")
#### 2.4.4.1. Addition -> +
```python
print(10 + 3)
```
#### 2.4.4.2. Subtraction -> -
```python
print(10 - 3)
```
#### 2.4.4.3. Multiplication -> *
```python
print(10 * 3)
```
#### 2.4.4.4. Division: Float value -> /
```python
print(10 / 3)
```
#### 2.4.4.5. Division: Integer value -> //
```python
print(10 // 3)
```
#### 2.4.4.6. Remainder -> %
```python
print(10 % 3)
```
#### 2.4.4.7. Exponent -> **  (10 ** 3  -> 1000 ```python
{retuns 3 to the power of 10 value = 1000})
```python
print(10 ** 3)
```
#### 2.4.4.8. TODO: Mrunal : What is it?
```python
print(10 ^ 3)
```
#### 2.4.4.9. TODO: Mrunal : What is it?
```python
print(10 & 3)
```

### 2.4.5. Incrementation tradtional and augmented assignment operator

#### 2.4.5.1. Incrementation tradtional
```python
num = 7
num = num + 3
print(num)
```

#### 2.4.5.2. Augmented assignment operator (Addition operations) (or increment)
```python
num = 7
num += 3
print(num)
```

#### 2.4.5.3. Augmented assignment operator (Subtraction operations) (or decrement)
```python
num = 7
num -= 3
print(num)
```

#### 2.4.5.4. Augmented assignment operator (Multiplication operations)
```python
num = 7
num *= 3
print(num)
```

#### 2.4.5.5. Augmented assignment operator (Division operations)
```python
num = 7
num /= 3
print(num)
```

#### 2.4.5.6. Augmented assignment operator (Division operations)
```python
num = 7
num //= 3
print(num)
```

#### 2.4.5.7. Augmented assignment operator (Remainder operations)
```python
num = 7
num %= 3
print(num)
```

#### 2.4.5.8. Augmented assignment operator (Exponent operations)
```python
num = 7
num **= 3
print(num)
```
#### 2.4.5.9. Incorrect : common mistake
```python
num = 6
num = + 3
print(num)
```

### 2.4.6. Number functions also called as number methods

#### 2.4.6.1. round
```python
print(round(2.976))
# absolute value
print(abs(2.976))
print(abs(-2.976))
```

#### 2.4.6.2. math module/object
```python
# need to import the module
import math
print(math.ceil(2.2))
```

#### 2.4.6.3. TODO: Munal : Add more math functions


## 2.5. Input function and type conversion

### 2.5.1. Input function -> Always saves as string object/datatype
```python
# Following prints "Please enter a number:" on terminal and save the input by user into num variable
name = input("Please enter a name:")
num = input("num:")
print(f"Name: {name} and num: {num}")

# Identify type with type() function
print(
    f"""Value and Type from input function:
    Name(value): {name} , Name(type): {type(name)} and
    Num(value): {num} , Num(type): {type(num)}""")
```


### 2.5.2. Type conversion

#### 2.5.2.1. Convert to string value
```python
print(str(24))
print(str(24.987))
print(str(True))
```

#### 2.5.2.2. Convert to integer / int value
```python
print(int("24"))
print(int(24.987))
print(int(True))
```

#### 2.5.2.3. Convert to float value
```python
print(float("24"))
print(float(24))
print(float(True))
```

#### 2.5.2.4. Convert to boolean / bool value
```python
print(bool("24"))
print(bool(24))
print(bool(24.987))
print(bool("True"))
print(bool(True))
print(bool("False"))
print(bool(False))
```

#### 2.5.2.5. Bool value converstion have falsey values (#Tags: #False, #Falsey, #Falsy)

+ "" -> Empty
```python
print(bool(""))
```

+ 0 -> zero (numeric)
```python
print(bool(0))
```

+ None -> none (not defined)
```python
print(bool(None))
```


# 3. Control Flow

## 3.1. comparision operators (with numbers)
### 3.1.1. greater than -> >
```python
print(10 > 3)
```
### 3.1.2. greater than or equal to -> >=
```python
print(10 >= 3)
```
### 3.1.3. less than -> <
```python
print(10 < 3)
```
### 3.1.4. less than or equal to -> <=
```python
print(10 <= 3)
```
### 3.1.5. equal to -> ==
```python
print(10 == 3)
print(10 == "10")
```
### 3.1.6. not equal to -> !=
```python
print(10 != 3)

```
### 3.1.7. order -> Unicode value -> ord
```python
print(ord("0"))
print(ord("9"))
print(ord("A"))
print(ord("Z"))
print(ord("a"))
print(ord("z"))
```

## 3.2. comparision operators (with strings)

### 3.2.1. greater than -> >
```python
print("bag" > "apple")
print("bag" > "bat")
print("bag" > "bad")
```
### 3.2.2. greater than or equal to -> >=
```python
print("bag" >= "apple")
```
### 3.2.3. less than -> <
```python
print("bag" < "apple")
print("bag" < "bat")
print("bag" < "bad")
```
### 3.2.4. less than or equal to -> <=
```python
print("bag" <= "apple")
```
### 3.2.5. equal to -> ==
```python
print("bag" == "apple")
print("bag" == "bag")
print("bag" == "Bag")
print("bag" == "BAG")
print("bag" == "baG")
print("bAg" == "baG")
```
### 3.2.6. not equal to -> !=
```python
print("bag" != "apple")
```

## 3.3. Print all unicode values         (#Tags: #All_Unicode_Values, #Unicode, #Unicode_Values)
+ Ref: https://stackoverflow.com/questions/33043212/printing-all-unicode-characters-in-python/33043329
```python
import sys

txtfile = "unicode_table.txt"
print("creating file: " + txtfile)
F = open(txtfile, "w", encoding="utf-16", errors='ignore')

line = f"Hex\t\t| Decimal\t\t| Value"
print(line, file=F)

for uc in range(sys.maxunicode):
    # line = "%s %s" % (hex(uc), chr(uc))
    line = f"{hex(uc)}\t\t| {int(uc)}\t\t| {chr(uc)}"
    print(line, file=F)
F.close()
```

## 3.4. Conditional statements
if statement (indentation define that it will be executed if expression is True)
+ syntax

```python
if <expression>:
  <statement1>            # executed if <expression> is True
  <statement2>            # executed if <expression> is True
<statement3>              # executed irrespective of if <expression>
```

### 3.4.1. if statement
```python
temperature = 40
if temperature > 35:
    print("High")
```

### 3.4.2. elif statement
```python
temperature = 10
if temperature > 35:
    print("High")
elif temperature < 15:
    print("Low")
elif temperature > 15 and temperature < 35:
    print("Normal")
```

### 3.4.3. if...elif...else statement
```python
temperature = 40
temperature = 10
temperature = 15
temperature = 35
if temperature > 35:
    print("High")
elif temperature < 15:
    print("Low")
elif temperature > 15 and temperature < 35:
    print("Normal")
else:
    print("Invalid value")
```

## 3.5. Ternary Operator           (#Tags: #If_Shorthand, #Short_Hand)

### 3.5.1. Example 1: if
+ Convert traditional if statement into ternary operator
```python
temperature = 40
if temperature > 35:
    message = "High"
else:
    message = "Low"
print(message)
```

+ Cleaner way of writing the code with the help of ternary operator
```python
temperature = 40
message = "High" if temperature > 35 else "Low"
print(message)
```

### 3.5.2. Example 2: nested if
+ Convert traditional if statement into ternary operator
```python
temperature = 40
if temperature == 30:
  message = "Normal"
else:
  if temperature > 35:
      message = "High"
  else:
      message = "Low"
print(message)
```

+ Cleaner way of writing the code with the help of ternary operator
```python
temperature = 40
message = "Normal" if temperature == 30 else "High" if temperature > 35 else "Low"
print(message)
```

### 3.5.3. TODO: Mrunal : Check whether elif is allowed ??? (Possibly No)


## 3.6. Chaining comparision Operator   (#Tags: #Comparsion_Operator_Shorthand, #shorthand)

### 3.6.1. Example: if
+ Convert traditional if statement into Chaining comparision operator
```python
temperature = 30
if temperature > 15 and temperature < 35:
    message = "Normal"
print(message)
```

+ Cleaner way of writing the code (more like mathematical/logical way) with the help of Chaining comparision operator
```python
temperature = 30
if 15 < temperature < 35:
    message = "Normal"
print(message)
```

## 3.7. Logical Operator (and , or , not)
```python
high_income = False
good_credit = True
student = "Munna Bhai IT"
course = "Python"
if (high_income or good_credit) and \
      (student == "Munna Bhai IT") and \
      (not course == "Java"):
    message = "Match"
else:
    message = "No Match"
print(message)
```

## 3.8. Short-circuit Evaluation (Logical operators are short-circuit -> if one of the operends {of logical operator} is false, evalutation stops)
```python
high_income = False
good_credit = True
student = "Munna Bhai IT"
course = "Python"
if (high_income or good_credit) and \
      (student == "Munna Bhai IT") and \
      (not course == "Java"):
    message = "Match"
else:
    message = "No Match"
print(message)
```

## 3.9. for Loops
### 3.9.1. Repeat n number of times
+ first argument -> number of iterations (Negative or 0 will end in 0 iterations)
  + starting value -> 0
  + ending value -> n-1
```python
for number in range(5):
    print(f"Attempt {number} {number * '*'}")
```

### 3.9.2. Repeat between 2 values
+ first argument -> staring number
+ second argument -> ending value (highest value not considered; less than 1 is considered)
```python
for number in range(1, 5):
    print(f"Attempt {number} {number * '*'}")
```

### 3.9.3. Repeat between 2 values with different increment/decrement value than default(+1/-1) as step
+ first argument -> staring number
+ second argument -> ending value (highest value not considered; less than 1 is considered)
+ third argument -> step (increment/decrement value) {default: +1}
```python
for number in range(1, 5, 2):
    print(f"Attempt {number} {number * '*'}")
```

### 3.9.4. Repeat with negative increment value (decrement value)
```python
for number in range(-1, -15, -2):
    print(f"Attempt {number} {number * '*'}")
```

## 3.10. for...else Loops and break statements            (#Tags: #Loop_Break, #Break)
###   3.10.1. comes out of the loop -> no more iterations break and goto next statements following loop
```python
number_value = 3
# Set value to True to execute break statement and False to execute else statement
found = False
for number in range(15):
    print(f"Attempt {number}")
    if number == number_value and found:
        print(f"Found hence exiting. No more iterations")
        break
else:
    print(f"Attempted {number} times and failed. Loop iterations finished.")
```

## 3.11. Nested loops -> Loops into loops
### 3.11.1. outer loop
```python
for x in range(5):
    # inner loop (executes 3 times for each value of outer loop)
    for y in range(3):
        print(f"Coordinate: ({x}, {y})")
```

## 3.12. Iterables -> objects that doesn't need explicit increment of variables
```python
print("\n*** Iterables ***")
print(type(5))
print(type(range(5)))
```

### 3.12.1. Iterables with range (range -> complex type)
```python
for value in range(5):
    print(f"value: {value}")
```

### 3.12.2. Iterables with string (string -> primitive type)
```python
for value in "Python Programming":
    print(f"value: {value}")
```

### 3.12.3. Iterables with array (array -> complex type)
```python
for value in [1, 2, 3, 4, 5, ]:
    print(f"value: {value}")
```

## 3.13. While loops
+ syntax
```python
while <expression>:
    <statement1>
    <statement2>
    <statement3>
```

+ Example
```python
number = 100
while number > 0:
    print(f"number: {number}")
    number //= 2

command = ""
while command.strip().lower() != "quit":
    print(f"command: {command}")
    command = input(">")
    print("Echo", command)
```

## 3.14. Infinte loops -> Infinte iterations            (#Tags: #Infinte_Loop, #Bad_Practice)
```python
while True:
    command = input(">")
    print("Echo", command)
    if command.strip().lower() == "quit":
        break
```

## 3.15. Exercise
+ Get output as following using even number logic and range(1,10)
```
2
4
6
8
We have 4 even numbers
```

### 3.15.1. Solution
```python
count = 0
for number in range(1, 10):
    if number % 2 == 0:
        print(number)
        count += 1
print(f"We have {count} even numbers")
```