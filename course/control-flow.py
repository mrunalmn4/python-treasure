# Sec: 3 -> Control Flow -> comparision operators (with numbers and strings)
import sys
print("\n*** comparision operators (with numbers and strings) ***")
# comparision operators (with numbers)
# greater than -> >
print(10 > 3)
# greater than or equal to -> >=
print(10 >= 3)
# less than -> <
print(10 < 3)
# less than or equal to -> <=
print(10 <= 3)
# equal to -> ==
print(10 == 3)
print(10 == "10")
# not equal to -> !=
print(10 != 3)

# order -> Unicode value -> ord
print(ord("0"))
print(ord("9"))
print(ord("A"))
print(ord("Z"))
print(ord("a"))
print(ord("z"))

# comparision operators (with strings)
# greater than -> >
print("bag" > "apple")
print("bag" > "bat")
print("bag" > "bad")
# greater than or equal to -> >=
print("bag" >= "apple")
# less than -> <
print("bag" < "apple")
print("bag" < "bat")
print("bag" < "bad")
# less than or equal to -> <=
print("bag" <= "apple")
# equal to -> ==
print("bag" == "apple")
print("bag" == "bag")
print("bag" == "Bag")
print("bag" == "BAG")
print("bag" == "baG")
print("bAg" == "baG")
# not equal to -> !=
print("bag" != "apple")


# print all unicode values         (#Tags: #All_Unicode_Values, #Unicode, #Unicode_Values)
# Ref: https://stackoverflow.com/questions/33043212/printing-all-unicode-characters-in-python/33043329
txtfile = "unicode_table.txt"
print("creating file: " + txtfile)
F = open(txtfile, "w", encoding="utf-16", errors='ignore')
line = f"Hex\t\t| Decimal\t\t| Value"
print(line, file=F)
for uc in range(sys.maxunicode):
    # line = "%s %s" % (hex(uc), chr(uc))
    line = f"{hex(uc)}\t\t| {int(uc)}\t\t| {chr(uc)}"
    print(line, file=F)
F.close()


# Sec: 3 -> Control Flow -> Conditional statements
print("\n*** Conditional statements ***")
# if statement(indentation define that it will be executed if expression is True)
# syntax
# if < expression >:
#     <statement1 >            # executed if <expression> is True
#     <statement2 >            # executed if <expression> is True
# <statement3 >              # executed irrespective of if <expression>

# if statement
temperature = 40
if temperature > 35:
    print("High")

# elif statement
temperature = 10
if temperature > 35:
    print("High")
elif temperature < 15:
    print("Low")
elif temperature > 15 and temperature < 35:
    print("Normal")

# if...elif...else statement
temperature = 40
temperature = 10
temperature = 15
temperature = 35
if temperature > 35:
    print("High")
elif temperature < 15:
    print("Low")
elif temperature > 15 and temperature < 35:
    print("Normal")
else:
    print("Invalid value")


# Sec: 3 -> Control Flow -> Ternary Operator           (#Tags: #If_Shorthand, #Short_Hand)
print("\n*** Ternary Operator ***")
# Example 1: if
# Convert traditional if statement into ternary operator
temperature = 40
if temperature > 35:
    message = "High"
else:
    message = "Low"
print(message)
# Cleaner way of writing the code with the help of ternary operator
temperature = 40
message = "High" if temperature > 35 else "Low"
print(message)

# Example 2: nested if
# Convert traditional if statement into ternary operator
temperature = 40
if temperature == 30:
    message = "Normal"
else:
    if temperature > 35:
        message = "High"
    else:
        message = "Low"
print(message)

# Cleaner way of writing the code with the help of ternary operator
temperature = 40
message = "Normal" if temperature == 30 else "High" if temperature > 35 else "Low"
print(message)

# TODO: Mrunal : Check whether elif is allowed ???


# Sec: 3 -> Control Flow -> Chaining comparision Operator   (#Tags: #Comparsion_Operator_Shorthand, #shorthand)
print("\n*** Chaining comparision Operator ***")
# Example: if
# Convert traditional if statement into Chaining comparision operator
temperature = 30
if temperature > 15 and temperature < 35:
    message = "Normal"
print(message)
# cleaner way of writing the code (more like mathematical/logical way) with the help of Chaining comparision operator
temperature = 30
if 15 < temperature < 35:
    message = "Normal"
print(message)


# Sec: 3 -> Control Flow -> Logical Operator (and , or , not)
# print("\n*** Logical Operator (and , or , not) ***")
high_income = False
good_credit = True
student = "Munna Bhai IT"
course = "Python"
if (high_income or good_credit) and \
        student == "Munna Bhai IT" and \
        (not course == "Java"):
    message = "Match"
else:
    message = "No Match"
print(message)


# Sec: 3 -> Control Flow -> Short-circuit Evaluation (Logical operators are short-circuit -> if one of the operends {of logical operator} is false, evalutation stops)
print(
    "\n*** Logical operators are short-circuit -> if one of the operends {of logical operator} is false, evalutation stops ***")
high_income = False
good_credit = True
student = "Munna Bhai IT"
course = "Python"
if (high_income or good_credit) and \
        (student == "Munna Bhai IT") and \
        (not course == "Java"):
    message = "Match"
else:
    message = "No Match"
print(message)


# Sec: 3 -> Control Flow -> for Loops
print("\n*** for Loops ***")
# Repeat n number of times
#   first argument -> number of iterations (Negative or 0 will end in 0 iterations)
#       starting value -> 0
#       ending value -> n-1
for number in range(5):
    print(f"Attempt {number} {number * '*'}")
# Repeat between 2 values
#   first argument -> staring number
#   second argument -> ending value (highest value not considered; less than 1 is considered)
for number in range(1, 5):
    print(f"Attempt {number} {number * '*'}")
# Repeat between 2 values with different increment/decrement value than default(+1/-1) as step
#   first argument -> staring number
#   second argument -> ending value (highest value not considered; less than 1 is considered)
#   third argument -> step (increment/decrement value) {default: +1}
for number in range(1, 5, 2):
    print(f"Attempt {number} {number * '*'}")
# Repeat with negative increment value (decrement value)
for number in range(-1, -15, -2):
    print(f"Attempt {number} {number * '*'}")


# Sec: 3 -> Control Flow -> for...else Loops and break statements            (#Tags: #Loop_Break, #Break)
print("\n*** for...else Loops and break statements ***")
#   comes out of the loop -> no more iterations break and goto next statements following loop
number_value = 3
# Set value to True to execute break statement and False to execute else statement
found = False
for number in range(15):
    print(f"Attempt {number}")
    if number == number_value and found:
        print(f"Found hence exiting. No more iterations")
        break
else:
    print(f"Attempted {number} times and failed. Loop iterations finished.")


# Sec: 3 -> Control Flow -> Nested loops -> Loops into loops
print("\n*** Nested loops ***")
# outer loop
for x in range(5):
    # inner loop (executes 3 times for each value of outer loop)
    for y in range(3):
        print(f"Coordinate: ({x}, {y})")


# Sec: 3 -> Control Flow -> Iterables -> objects that doesn't need explicit increment of variables
print("\n*** Iterables ***")
print(type(5))
print(type(range(5)))

# Iterables with range (range -> complex type)
for value in range(5):
    print(f"value: {value}")

# Iterables with string (string -> primitive type)
for value in "Python Programming":
    print(f"value: {value}")

# Iterables with array (array -> complex type)
for value in [1, 2, 3, 4, 5, ]:
    print(f"value: {value}")


# Sec: 3 -> Control Flow -> While loops
# print("\n*** While loops ***")
# syntax
# while <expression>:
#     <statement1>
#     <statement2>
#     <statement3>

number = 100
while number > 0:
    print(f"number: {number}")
    number //= 2

command = ""
while command.strip().lower() != "quit":
    print(f"command: {command}")
    command = input(">")
    print("Echo", command)


# Sec: 3 -> Control Flow -> Infinte loops -> Infinte iterations            (#Tags: #Infinte_Loop, #Bad_Practice)
print("\n*** Infinte loops ***")
while True:
    command = input(">")
    print("Echo", command)
    if command.strip().lower() == "quit":
        break

# Sec: 3 -> Control Flow -> Exercise
# Get output as following using even number logic and range(1,10)
# 2
# 4
# 6
# 8
# We have 4 even numbers
# Solution
count = 0
for number in range(1, 10):
    if number % 2 == 0:
        print(number)
        count += 1
print(f"We have {count} even numbers")
