
# import module: math
import math

# Comments -> These are comments in python
#   Full line comment
# print("Test")               # In line comment

# Calling a function -> <function_name>(<arguments>)

# print function
# prints -> Hello World!!!
print("\n)*** Basic Hello World using print function ***")
print("Hello World!!!")
# prints -> $$$$$
print("$" * 5)


# variables

# Sec: 2 -> Primitive types (Premitive datatypes) -> variables

print("\n)*** Primitive types (Premitive datatypes) -> define and initialize variables ***")
# integer variable
student_count = 1000
# floting point variable
rating = 4.99
# boolean variable (accepts True or False -> These are case sensitive)
is_published = True
# string variable (must use single{'abc'} or multiple{"abc"} quotes and for multi-line string value triple single{'''abc'''} or triple double{"""abc"""} qoutes must be used)
course_name = 'Python Programming'
course_name = "Python Programming"
mail_body = '''
This is
an example
to show
multi-line string value
'''
mail_body = """
This is
an example
to show
multi-line string value
"""

# Sec: 2 -> Primitive types (Premitive datatypes) -> variable names
#   must be meaningful and descriptive
#   must start with small character(alphabets) and keep everything in small case
#   seperate words using underscode "_"
#   space around equal to sign " = "


# Sec: 2 -> Primitive types (Premitive datatypes) -> string
course = "Python programming"

# Length of string
print("\n)*** Length of string ***")
print(len(course))

# Specific character via index
#   Left -> -> Right : index starts front/left with 0 and then 1 then 2 and so on till 1 less than the length of string
#   Left <- <- Right : index starts behind/right with -1 and then -2 then -3 and so on till 0
print("\n*** Specific character via index ***")
print(course[0])
print(course[1])
print(course[5])
print(course[-12])
print(course[-5])
print(course[-1])

# Slice -> substring -> extracting substring from the string -> remember highest value of right side of colon is not considered in selection means ([5:9] -> 9 index will not be considered in selection)
print("\n*** Slice -> substring -> extracting substring from the string ***")
# First 5 characters
print(course[0:5])
print(course[1:5])
print(course[2:])
print(course[:8])
print(course[:])


# Sec: 2 -> Primitive types (Premitive datatypes) -> Escape sequences
print("\n*** Escape sequences ***")
#   Escape character \ and Escape sequence \" -> prints "
print("Python \"programming")
#   Escape character \ and Escape sequence \' -> prints '
print("Python \'programming")
#   Escape character \ and Escape sequence \\ -> prints \\
print("Python \\programming")
#   Escape character \ and Escape sequence \n -> prints new line
print("Python \nprogramming")
#   Escape character \ and Escape sequence \t -> prints tab
print("Python \t\tprogramming")


# Sec: 2 -> Primitive types (Premitive datatypes) -> Formatted strings
print("\n*** Formatted strings ***")
first = "Munna"
last = "Bhai IT"
# concatination : tradional approach
print(first + " " + last)
# Formatted strings -> Use parenthesis brackets { } to include variables function or expression
print(f"{first} {last}")
print(f"{first} {last} {len(first)} {5 + 9} { 9 <= 8}")


# Sec: 2 -> Primitive types (Premitive datatypes) -> string functions also called as string methods
print("\n*** string functions also called as string methods ***")
course = " Python programMing   "
# converts in upper case
print(course.upper())
# converts in lower case
print(course.lower())
# converts in title case
print(course.title())
# converts in inverse/swap the exsiting case
print(course.swapcase())
# converts in strips/trims empty spaces on both the sides
print(course.strip())
# converts in strips/trims empty spaces on the left sides
print(course.lstrip())
# converts in strips/trims empty spaces on the right sides
print(course.rstrip())
# returns -1 if found prints index of first character
print(course.find("pro"))
# returns -1 if not found (Not found because python is case sensitive)
print(course.find("Pro"))
# replaces the character with the another one
print(course.replace("p", "j"))
# returns True/False if found or not found respectively
print("pro" in course)
# returns False/True if found or not found respectively
print("pro" not in course)
# returns array of words as elements which are space seperated
print(course.split())


# Sec: 2 -> Primitive types (Premitive datatypes) -> Numbers
print("\n*** Numbers ***")
# Integer
number_integer = 34
# Float
number_float = 34.2346
# Complex number
number_complex = 34 + 23j
print(
    f"integer:{number_integer} \tfloat:{number_float} \tcomplex:{number_complex}")

# Standard Arithematic operations
print("\n*** Standard Arithematic operations ***")
# Addition -> +
print(10 + 3)
# Subtraction -> -
print(10 - 3)
# Multiplication -> *
print(10 * 3)
# Division: Float value -> /
print(10 / 3)
# Division: Integer value -> //
print(10 // 3)
# Remainder -> %
print(10 % 3)
# Exponent -> **  (10 ** 3  -> 1000 {retuns 3 to the power of 10 value = 1000})
print(10 ** 3)
# TODO: Mrunal : ?
print(10 ^ 3)
# TODO: Mrunal : ?
print(10 & 3)

# Incrementation tradtional and augmented assignment operator
print("\n*** Incrementation tradtional and augmented assignment operator ***")
# Incrementation tradtional
num = 7
num = num + 3
print(num)
# Augmented assignment operator (Addition operations)
num = 7
num += 3
print(num)
# Augmented assignment operator (Subtraction operations)
num = 7
num -= 3
print(num)
# Augmented assignment operator (Multiplication operations)
num = 7
num *= 3
print(num)
# Augmented assignment operator (Division operations)
num = 7
num /= 3
print(num)
# Augmented assignment operator (Division operations)
num = 7
num //= 3
print(num)
# Augmented assignment operator (Remainder operations)
num = 7
num %= 3
print(num)
# Augmented assignment operator (Exponent operations)
num = 7
num **= 3
print(num)

# Incorrect : common mistake
num = 6
num = + 3
print(num)


# Sec: 2 -> Primitive types (Premitive datatypes) -> number functions also called as number methods
print("\n*** number functions also called as number methods ***")
# round
print(round(2.976))
# absolute value
print(abs(2.976))
print(abs(-2.976))

# math module/object
#   need to import the module
# import math
print(math.ceil(2.2))
# TODO: Munal : Add more math functions


# Sec: 2 -> Primitive types (Premitive datatypes) -> input function and type conversion
print("\n*** input function and type conversion ***")
# Input function -> Always saves as string object/datatype
# Following prints "Please enter a number:" on terminal and save the input by user into num variable
name = input("Please enter a name:")
num = input("num:")
print(f"Name: {name} and num: {num}")

# Identify type with type() function
print(
    f"""Value and Type from input function:
    Name(value): {name} , Name(type): {type(name)} and
    Num(value): {num} , Num(type): {type(num)}""")

# Type conversion
#   Convert to string value
print(str(24))
print(str(24.987))
print(str(True))
#   Convert to integer / int value
print(int("24"))
print(int(24.987))
print(int(True))
#   Convert to float value
print(float("24"))
print(float(24))
print(float(True))
#   Convert to boolean / bool value
print(bool("24"))
print(bool(24))
print(bool(24.987))
print(bool("True"))
print(bool(True))
print(bool("False"))
print(bool(False))
#       Bool value converstion have falsey values (#Tags: #False, #Falsey, #Falsy)
#           "" -> Empty
print(bool(""))
#           0 -> zero (numeric)
print(bool(0))
#           None -> none (not defined)
print(bool(None))
